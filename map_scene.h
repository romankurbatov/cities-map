#ifndef MAPSCENE_H
#define MAPSCENE_H

#include <QtMath>
#include <QGraphicsScene>
#include <QStaticText>
#include <QVector>
#include <QPointF>

class MapScene : public QGraphicsScene
{
    Q_OBJECT

public:
    MapScene(QObject *parent = nullptr);

protected:
    void drawBackground(QPainter *painter, const QRectF &rect) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void wheelEvent(QGraphicsSceneWheelEvent *wheelEvent) override;

private:
    void loadCities();

    QPointF sceneVectorToInternal(const QPointF &sceneVector);
    QPointF internalVectorToScene(const QPointF &internalVector);
    QPointF scenePosToInternal(const QPointF &scenePos);
    QPointF internalPosToScene(const QPointF &internalPos);

    void verifyMapPosition();

    // We use internal coordinates, which are like geographic,
    // but scaled to [0,1], and latitude is inverse

    struct City {
        QStaticText label;
        QPointF internalPos; // in internal coordinates
        char priority, minLevel, maxLevel;
    };

    QVector<City> mCities;
    int mLevel;
    QPointF mTopLeftInternalPos; // in internal coordinates

    constexpr static const int mHeight = 500;
    constexpr static const int mWidth = 2*mHeight; // for simplicity


    // Ratio: distance in scene coordinates / distance in internal coordinates when mLevel = 0
    // For simplicity, we choose them to see exactly whole map when mLevel = 20
    static const double mBaseScaleX;
    static const double mBaseScaleY;

    constexpr static const int mMinLevel = 0;
    constexpr static const int mMaxLevel = 20;
};

#endif // MAPSCENE_H
