TEMPLATE = app
TARGET = cities-map
CONFIG += c++17 link_pkgconfig qt
PKGCONFIG += libmongocxx
QT += widgets

SOURCES += \
    main.cpp \
    map_scene.cpp

HEADERS += \
    map_scene.h
