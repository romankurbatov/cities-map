#include <iostream>

#include <QApplication>
#include <QGraphicsView>

#include "map_scene.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    MapScene scene;
    QGraphicsView view(&scene, nullptr);
    view.setFixedSize(scene.width() + 2*view.frameWidth(),
                      scene.height() + 2*view.frameWidth());
    view.show();

    return app.exec();
}
