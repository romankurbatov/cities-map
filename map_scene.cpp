#include "map_scene.h"

#include <iostream>
#include <algorithm>

#include <bsoncxx/document/view.hpp>
#include <bsoncxx/document/element.hpp>
#include <bsoncxx/stdx/string_view.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/database.hpp>
#include <mongocxx/collection.hpp>
#include <mongocxx/cursor.hpp>

#include <Qt>
#include <QtMath>
#include <QList>
#include <QPainter>
#include <QGraphicsSceneEvent>
#include <QSharedPointer>

const double MapScene::mBaseScaleX = (mWidth - 1)*qPow(2, 20);
const double MapScene::mBaseScaleY = (mHeight - 1)*qPow(2, 20);

MapScene::MapScene(QObject *parent) :
    QGraphicsScene(0, 0, mWidth, mHeight, parent),
    mLevel(20),
    mTopLeftInternalPos(0, 0)
{
    loadCities();
}

void MapScene::drawBackground(QPainter *painter, const QRectF &rect) {
    // Indentations between city point and label
    static const double xIndent = 2;
    static const double yIndent = 1;

    // Radius of city point
    static const double radius = 2;

    painter->setBrush(Qt::SolidPattern);
    painter->setRenderHints(QPainter::Antialiasing);

    QVector<QRectF> addedCityLabelRects;

    for (const City &city : mCities) {
        if (city.minLevel <= mLevel && mLevel <= city.maxLevel) {
            QPointF scenePos = internalPosToScene(city.internalPos);
            if (rect.contains(scenePos)) {
                QPointF labelAnchor(scenePos.x() + xIndent,
                    scenePos.y() - city.label.size().height() - yIndent);

                // If label intersects scene borders, try another positions
                if (labelAnchor.x() + city.label.size().width() > mWidth) {
                    labelAnchor.setX(scenePos.x() - city.label.size().width() - xIndent);
                }
                if (labelAnchor.y() < 0) {
                    labelAnchor.setY(scenePos.y() + yIndent);
                }
                QRectF labelRect(labelAnchor, city.label.size());

                bool intersects = false;
                for (const QRectF &anotherLabelRect : addedCityLabelRects) {
                    if (labelRect.intersects(anotherLabelRect)) {
                        intersects = true;
                        break;
                    }
                }
                if (intersects) {
                    continue;
                }

                painter->drawEllipse(scenePos, radius, radius);
                painter->drawStaticText(labelAnchor, city.label);
                addedCityLabelRects.append(labelRect);
            }
        }
    }
}

void MapScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    if (mouseEvent->buttons() == Qt::LeftButton) {
        QPointF oldTopLeftInternalPos = mTopLeftInternalPos;

        QPointF sceneVector = mouseEvent->scenePos() - mouseEvent->lastScenePos();

        mTopLeftInternalPos -= sceneVectorToInternal(sceneVector);
        verifyMapPosition();

        if (mTopLeftInternalPos != oldTopLeftInternalPos) {
            invalidate();
        }
    }
}

void MapScene::wheelEvent(QGraphicsSceneWheelEvent *wheelEvent) {
    QPointF internalPos = scenePosToInternal(wheelEvent->scenePos());

    static const int step = 120; // See QGraphicsSceneWheelEvent::delta() reference manual
    int oldLevel = mLevel;
    mLevel -= wheelEvent->delta()/step;
    mLevel = qMax(mLevel, mMinLevel);
    mLevel = qMin(mLevel, mMaxLevel);

    if (mLevel == oldLevel) {
        return;
    }

    // We want to try to set the position of the map so that
    // internal position of mouse cursor does not change
    mTopLeftInternalPos = internalPos - sceneVectorToInternal(wheelEvent->scenePos());
    verifyMapPosition();
    invalidate();
}

void MapScene::loadCities() {
    static const double minLongtitude = -180;
    static const double maxLongtitude = 180;
    static const double minLatitude = -90;
    static const double maxLatitude = 90;

    mongocxx::instance instance;
    mongocxx::uri uri("mongodb://localhost:27017");
    mongocxx::client client(uri);
    mongocxx::database meteodb = client["meteodb"];
    mongocxx::collection cities = meteodb["cities"];
    mongocxx::cursor cursor = cities.find({});

    setFont(QFont("Liberation Sans"));

    for (const auto &doc : cursor) {
        QString name = doc["name"].get_utf8().value.data();
        auto coordinates = static_cast<bsoncxx::array::view>
                (doc["location"]["coordinates"].get_array());
        double longtitude = coordinates[0].get_double().value;
        double latitude = coordinates[1].get_double().value;
        char priority = doc["priority"].get_int32().value;
        char minLevel = doc["min_level"].get_int32().value;
        char maxLevel = doc["max_level"].get_int32().value;
        City city = {
            QStaticText(name),
            QPointF((longtitude-minLongtitude)/(maxLongtitude-minLongtitude),
                    (maxLatitude-latitude)/(maxLatitude-minLatitude)),
            priority,
            minLevel, maxLevel
        };
        city.label.prepare(QTransform(), font());
        mCities.append(city);
    }

    std::sort(mCities.begin(), mCities.end(),
        [](const City &left, const City &right) {
            return left.priority > right.priority;
    });
}

QPointF MapScene::sceneVectorToInternal(const QPointF &sceneVector) {
    return QPointF(
        sceneVector.x()/(mBaseScaleX/qPow(2, mLevel)),
        sceneVector.y()/(mBaseScaleY/qPow(2, mLevel)));
}

QPointF MapScene::internalVectorToScene(const QPointF &internalVector) {
    return QPointF(
        internalVector.x()*(mBaseScaleX/qPow(2, mLevel)),
        internalVector.y()*(mBaseScaleY/qPow(2, mLevel)));
}

QPointF MapScene::scenePosToInternal(const QPointF &scenePos) {
    return mTopLeftInternalPos + sceneVectorToInternal(scenePos);
}

QPointF MapScene::internalPosToScene(const QPointF &internalPos) {
    return internalVectorToScene(internalPos - mTopLeftInternalPos);
}

void MapScene::verifyMapPosition() {
    if (mTopLeftInternalPos.x() < 0) {
        mTopLeftInternalPos.setX(0);
    }
    if (mTopLeftInternalPos.y() < 0) {
        mTopLeftInternalPos.setY(0);
    }

    QPointF bottomRightInternalPos = scenePosToInternal(QPointF(mWidth - 1, mHeight - 1));
    if (bottomRightInternalPos.x() > 1) {
        mTopLeftInternalPos.setX(mTopLeftInternalPos.x() - (bottomRightInternalPos.x() - 1));
    }
    if (bottomRightInternalPos.y() > 1) {
        mTopLeftInternalPos.setY(mTopLeftInternalPos.y() - (bottomRightInternalPos.y() - 1));
    }
}
